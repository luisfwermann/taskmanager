<?php

class VerifyLogin
{

    const SESSION_VALUE_LOGGEDIN = 'loggedin';
    const COOKIES_VALUE_LOGGEDIN = 'Loggedin';

    public function verifyUserIsLoggedAndRedirect()
    {
        if ($this->isUserLogged()) {
            $this->redirectUser();
        }
    }

    private function isUserLogged()
    {
        return ($this->isUserLoggedInCookies() || $this->isUserLoggedInSession());
    }

    private function isUserLoggedInSession()
    {
        return (isset($_SESSION[SESSION_VALUE_LOGGEDIN]) && $_SESSION[SESSION_VALUE_LOGGEDIN] == true);
    }

    private function isUserLoggedInCookies()
    {
        return (isset($_COOKIE[COOKIES_VALUE_LOGGEDIN]) && $_COOKIE[COOKIES_VALUE_LOGGEDIN] == true);
    }

    private function redirectUser()
    {
        header("Location: http://www.google.com");
        exit();
    }
}