<?php

class MyTable {

    const CONFIG_SERVER = 'localhost';
    const CONFIG_USER = 'user';
    const CONFIG_PASSWORD = 'password';

    private function getNewConnection()
    {
        return new DatabaseConnection(CONFIG_SERVER, CONFIG_USER, PASSWORD);
    }

    public function query($sql)
    {
        return $this->getNewConnection()->query($sql);
    }
}