<?php

class Printer
{
    public function main() {
        for ($actualNumber = 1; $actualNumber <= 100; $actualNumber++) {
            $threeMultiplier = ($actualNumber % 3 == 0);
            $fiveMultiplier = ($actualNumber % 5 == 0);

            if ($threeMultiplier && $fiveMultiplier) {
                echo "FizzBuzz";
            } else if ($threeMultiplier) {
                echo "Fizz";
            } else if ($fiveMultiplier) {
                echo "Buzz";
            } else {
                echo $actualNumber;
            }
            echo "\n";
        }
    }
}