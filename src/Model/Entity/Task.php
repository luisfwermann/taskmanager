<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Task Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $priority
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Task extends Entity
{

    const PRIORIDADE_BAIXA = 1;
    const PRIORIDADE_MEDIA = 2;
    const PRIORIDADE_ALTA = 3;
    const PRIORIDADE_IMEDIATA = 4;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function getPrioritiesList() {
        return [
            self::PRIORIDADE_BAIXA => 'Baixa',
            self::PRIORIDADE_MEDIA => 'Média',
            self::PRIORIDADE_ALTA => 'Alta',
            self::PRIORIDADE_IMEDIATA => 'Imediata'
        ];
    }

    public function getPriorityLabel() {
        $priorities = $this->getPrioritiesList();
        return $priorities[$this->priority];
    }
}
