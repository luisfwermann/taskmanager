<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar Tarefa'), ['action' => 'edit', $task->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar Tarefa'), ['action' => 'delete', $task->id], ['confirm' => __('Are you sure you want to delete # {0}?', $task->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Tarefas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova Tarefa'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($task->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Cód.') ?></th>
            <td><?= $this->Number->format($task->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Título') ?></th>
            <td><?= h($task->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prioridade') ?></th>
            <td><?= h($task->getPriorityLabel()) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Criada em') ?></th>
            <td><?= h(date("d/m/Y H:i",strtotime($task->created))) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modificada em') ?></th>
            <td><?= h(date("d/m/Y H:i",strtotime($task->modified))) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descrição') ?></h4>
        <?= $this->Text->autoParagraph(h($task->description)); ?>
    </div>
</div>
