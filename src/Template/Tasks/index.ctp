<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Nova Tarefa'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tasks index large-9 medium-8 columns content">
    <h3><?= __('Tarefas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', 'Cód.') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title', 'Título') ?></th>
                <th scope="col"><?= $this->Paginator->sort('priority', 'Prioridade') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created', 'Criada em') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified', 'Modificada em') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tasks as $task): ?>
            <tr>
                <td><?= $this->Number->format($task->id) ?></td>
                <td><?= h($task->title) ?></td>
                <td><?= h($task->getPriorityLabel()) ?></td>
                <td><?= h(date("d/m/Y H:i",strtotime($task->created))) ?></td>
                <td><?= h(date("d/m/Y H:i",strtotime($task->modified))) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $task->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $task->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $task->id], ['confirm' => __('Você tem certeza de que quer deletara tarefa # {0}?', $task->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeiro')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
            <?= $this->Paginator->last(__('último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) do total de {{count}}')]) ?></p>
    </div>
</div>
