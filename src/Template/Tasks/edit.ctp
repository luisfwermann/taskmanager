<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Excluir'),
                ['action' => 'delete', $task->id],
                ['confirm' => __('Tem certeza de que deseja deletar a tarefa # {0}?', $task->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar Tarefas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="tasks form large-9 medium-8 columns content">
    <?= $this->Form->create($task) ?>
    <fieldset>
        <legend><?= __('Editar Tarefa') ?></legend>
        <?php
            echo $this->Form->input('title', ['label' => 'Título']);
            echo $this->Form->input('description', ['label' => 'Descrição']);
            echo $this->Form->label('Prioridade');
            echo $this->Form->select('priority', $task->getPrioritiesList(), ['label' => 'Prioridade']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Gravar')) ?>
    <?= $this->Form->end() ?>
</div>
