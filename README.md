------------------------------------------------------
PARA RODAR CORRETAMENTE: 
------------------------------------------------------

Clonar o projeto.
```
#!shell

git clone https://usuariobitbucket@bitbucket.org/luisfwermann/taskmanager.git
```

Criar um novo banco de dados no MySQL e rodar os sqls:
```
#!sql

CREATE TABLE tasks (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(50),
    description TEXT(50),
    priority INT,
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
);

INSERT INTO tasks (title, description, priority, created, modified) VALUES ('Ajustar relatório de fluxo de caixa', 'Verificar porque o fluxo de caixa está saindo com totalizadores zerados', 3, NOW(), NOW());
```

Configurar o banco de dados:
```
#!shell

cp taskmanager/config/app.default.php taskmanager/config/app.php
vim taskmanager/config/app.php

// Ajustar a parte de 'Datasources' do arquivo de configuração e ajustar os dados  de password, user e database conforme a base de dados criada.
```

Inicializar o servidor:
```
#!shell
// Dentro da pasta do projeto
bin/cake server

```

Agora é só acessar o endereço: http://localhost:8765/tasks

------------------------------------------------------
ALGUMAS COISAS IMPORTANTES QUE GOSTARIA DE INFORMAR: 
------------------------------------------------------

* As questões de refatoração estão na pasta "refatorados" dentro do app, estão separadas por questões.
* Programado no Ubuntu, utilizando o PHPStorm e o My SQL Workbench. 
* Utilizei o framework recomendado (CakePHP 3.0), não conhecia ele, achei bastante prático e simples.

Abraços!